import * as React from 'react';
import { Provider } from 'react-redux';
import { IModuleStore } from 'redux-dynamic-modules/lib';

import { ConnectedOffersView } from './components/OffersView';
import { configureOffersStore } from './redux/configureStore';
import { IOffersApplicationState } from './Contracts';

class App extends React.Component {
  private _store: IModuleStore<IOffersApplicationState>;
  constructor(props, context) {
    super(props, context);

    this._store = configureOffersStore();
  }

  public render() {
    return (
      <Provider store={this._store}>
        <>

          <ConnectedOffersView />
        </>
      </Provider >
    );
  }
}

export default App;
