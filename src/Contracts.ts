import { SortDirection, FieldKey } from './Constants';

export interface IOffersApplicationState extends IOffersAwareState { }

export interface IOffersAwareState {
    offersState: IOffersState;
}

export interface IOffersState {
    offerPages: IOffer[][];
    loading: boolean;
    pageErrors: Error[];
    sortField: FieldKey;
    sortDirection: SortDirection;
    currentPage: number;
}

export interface IOffer {
    miles: number;
    origin: IOrigin;
    destination: IDestination;
    offer: number;
}

export interface IOrigin {
    city: string;
    state: string;
    pickup: ITime;
}

export interface IDestination {
    city: string;
    state: string;
    dropoff: ITime;
}

export interface ITime {
    start: Date;
    end: Date;
}