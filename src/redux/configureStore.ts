import { IOffersApplicationState } from '../Contracts';
import { configureStore, IModuleStore } from "redux-dynamic-modules";
import { getSagaExtension } from "redux-dynamic-modules-saga";
import { getOffersModule } from './offers/OffersModule';

export function configureOffersStore(): IModuleStore<IOffersApplicationState> {
    const store = configureStore({}, [getSagaExtension()], getOffersModule());

    return store;
}