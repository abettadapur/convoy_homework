import { ActionsUnion } from "@martin_hotell/rex-tils";
import { createAction } from "../utils/createAction";
import { IOffer } from '../../Contracts';
import { FieldKey } from 'src/Constants';

export const enum OffersActionsTypes {
    Intialize = "Initialize",
    SortChanged = "SortChanged",
    OffersLoaded = "OffersLoaded",
    OffersLoadFailed = "OffersLoadFailed",
    PageLeft = "PageLeft",
    PageRight = "PageRight"
}

export const OffersActions = {
    initialize: () => createAction(OffersActionsTypes.Intialize),
    sortChanged: (sortField: FieldKey) => createAction(OffersActionsTypes.SortChanged, sortField),
    pageLeft: () => createAction(OffersActionsTypes.PageLeft),
    pageRight: () => createAction(OffersActionsTypes.PageRight),
    offersLoaded: (offers: IOffer[], page: number) => createAction(OffersActionsTypes.OffersLoaded, { page, offers }),
    offersLoadFailed: (error: Error, page: number) => createAction(OffersActionsTypes.OffersLoadFailed, { error, page })
};

export type OffersActions = ActionsUnion<typeof OffersActions>;