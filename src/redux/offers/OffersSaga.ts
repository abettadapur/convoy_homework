import { OffersActionsTypes, OffersActions } from './OffersActions';
import { SagaIterator } from 'redux-saga';
import { call, put, takeEvery, select } from 'redux-saga/effects';
import { getOffers } from './OffersApi';
import { IOffer } from '../../Contracts';
import { getSortField, getSortDirection, getCurrentPage, getCurrentPageOfOffers } from './OffersSelectors';
import { FieldKey, SortDirection } from 'src/Constants';

export function* offersSaga(): SagaIterator {
    yield takeEvery(OffersActionsTypes.Intialize, fetchInitialData);
    yield takeEvery(OffersActionsTypes.SortChanged, pageSortedData);
    yield takeEvery(OffersActionsTypes.PageLeft, pageSortedData);
    yield takeEvery(OffersActionsTypes.PageRight, pageSortedData);
}

export function* fetchInitialData(): SagaIterator {
    const offers: IOffer[] = yield call(getOffers);
    yield put(OffersActions.offersLoaded(offers, 0));
}

export function* pageSortedData(): SagaIterator {
    const sortField: FieldKey = yield select(getSortField);
    const sortDirection: SortDirection = yield select(getSortDirection);
    const currentPage: number = yield select(getCurrentPage);
    const currentOffers: IOffer[] = yield select(getCurrentPageOfOffers);

    try {
        if (currentOffers.length === 0) {
            const offers: IOffer[] = yield call(getOffers, { offset: currentPage * 20, order: sortDirection, sort: sortField });
            yield put(OffersActions.offersLoaded(offers, currentPage));
        }
    } catch (e) {
        yield put(OffersActions.offersLoadFailed(e, currentPage))
    }
}