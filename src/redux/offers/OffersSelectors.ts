import { IOffersAwareState, IOffer } from 'src/Contracts';
import { FieldKey, SortDirection } from 'src/Constants';

export function getCurrentPageOfOffers(state: IOffersAwareState): IOffer[] {
    return state.offersState.offerPages[state.offersState.currentPage] || [];
}

export function getCurrentPageError(state: IOffersAwareState): Error | undefined {
    return state.offersState.pageErrors[state.offersState.currentPage];
}

export function getSortField(state: IOffersAwareState): FieldKey {
    return state.offersState.sortField;
}

export function getSortDirection(state: IOffersAwareState): SortDirection {
    return state.offersState.sortDirection;
}

export function getCurrentPage(state: IOffersAwareState): number {
    return state.offersState.currentPage;
}
