import produce from "immer";
import { OffersActions, OffersActionsTypes } from './OffersActions';
import { IOffersState } from '../../Contracts';
import { FieldKey, SortDirection } from 'src/Constants';

export function offersReducer(state: IOffersState, action: OffersActions): IOffersState {
    return produce(state || getDefaultState(), (draft: IOffersState) => {
        switch (action.type) {
            case OffersActionsTypes.Intialize: {
                draft.loading = true;
                break;
            }

            case OffersActionsTypes.OffersLoaded: {
                const {
                    offers,
                    page
                } = action.payload;

                draft.offerPages[page] = offers;
                delete draft.pageErrors[page];
                draft.loading = false;
                break;
            }

            case OffersActionsTypes.OffersLoadFailed: {
                const {
                    page,
                    error
                } = action.payload;

                draft.pageErrors[page] = error;
                draft.loading = false;
                break;
            }

            case OffersActionsTypes.SortChanged: {
                const newSortField: FieldKey = action.payload;
                draft.loading = true;

                draft.offerPages = [];

                if (newSortField === draft.sortField) {
                    if (draft.sortDirection === SortDirection.asc) {
                        draft.sortDirection = SortDirection.desc;
                    } else {
                        draft.sortDirection = SortDirection.asc;
                    }
                } else {
                    draft.sortField = newSortField;
                    draft.sortDirection = SortDirection.asc;
                }

                break;
            }

            case OffersActionsTypes.PageLeft: {
                if (draft.currentPage > 0) {
                    draft.currentPage--;
                }

                if (!draft.offerPages[draft.currentPage]) {
                    draft.loading = true;
                }

                break;
            }

            case OffersActionsTypes.PageRight: {
                draft.currentPage++;
                if (!draft.offerPages[draft.currentPage]) {
                    draft.loading = true;
                }
            }
        }
    });
}

function getDefaultState(): IOffersState {
    return {
        offerPages: [],
        loading: false,
        pageErrors: [],
        sortField: FieldKey.pickupDate,
        sortDirection: SortDirection.desc,
        currentPage: 0,
    };
}