import { IOffer } from '../../Contracts';
import { FieldKey, SortDirection } from 'src/Constants';

const baseURL = "https://convoy-frontend-homework-api.herokuapp.com/";
export interface IOffersRequestOptions {
    offset?: number;
    limit?: number;
    sort?: FieldKey;
    order?: SortDirection;
}

export async function getOffers(options?: IOffersRequestOptions): Promise<IOffer[]> {
    const query = buildQueryString(options);

    const response = await fetch(`${baseURL}offers${query !== "" ? "?" + query : ""}`);
    const json: IOffer[] = await response.json();
    return json.map(parseOffer);
}

function buildQueryString(options?: IOffersRequestOptions) {
    if (!options) {
        return "";
    }

    let kvps: string[] = [];
    Object.keys(options).forEach(optionKey => {
        kvps.push(`${optionKey}=${options[optionKey]}`);
    });

    return kvps.join("&");
}

function parseOffer(offer: IOffer): IOffer {
    if (typeof offer.destination.dropoff.start === "string") {
        offer.destination.dropoff.start = new Date(offer.destination.dropoff.start);
    }

    if (typeof offer.destination.dropoff.end === "string") {
        offer.destination.dropoff.end = new Date(offer.destination.dropoff.end);
    }

    if (typeof offer.origin.pickup.start === "string") {
        offer.origin.pickup.start = new Date(offer.origin.pickup.start);
    }

    if (typeof offer.origin.pickup.end === "string") {
        offer.origin.pickup.end = new Date(offer.origin.pickup.end);
    }

    return offer;
}