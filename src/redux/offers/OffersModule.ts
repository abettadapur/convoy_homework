import { OffersActions } from './OffersActions';
import { ISagaModule } from "redux-dynamic-modules-saga";
import { IOffersAwareState } from '../../Contracts';
import { offersReducer } from './OffersReducer';
import { offersSaga } from './OffersSaga';

export function getOffersModule(): ISagaModule<IOffersAwareState> {
    return {
        id: "offers-module",
        reducerMap: {
            offersState: offersReducer
        },
        sagas: [offersSaga],

        initialActions: [OffersActions.initialize()]
    };
}