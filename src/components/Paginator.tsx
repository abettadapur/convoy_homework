import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import * as React from 'react';

export interface IPaginatorProps {
    pageNumber: number;
    onLeft: () => void;
    onRight: () => void;
}

export class Paginator extends React.Component<IPaginatorProps> {
    public render(): JSX.Element {
        return (
            <>
                <IconButton onClick={this.props.onLeft} disabled={this.props.pageNumber === 1}>
                    <Icon>keyboard_arrow_left</Icon>
                </IconButton>
                PAGE {this.props.pageNumber}
                <IconButton onClick={this.props.onRight}>
                    <Icon>keyboard_arrow_right</Icon>
                </IconButton>
            </>
        )
    }
}