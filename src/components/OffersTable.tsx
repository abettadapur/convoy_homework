import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import * as React from 'react';
import { FieldKey, SortDirection } from 'src/Constants';

import { IOffer, ITime } from '../Contracts';

export interface IOffersTableProps {
    offers: IOffer[];
    sortField: FieldKey;
    sortDirection: SortDirection;

    onSortToggled: (sortField: FieldKey) => void;
}

export class OffersTable extends React.Component<IOffersTableProps> {
    public render(): JSX.Element {
        return (
            <Table>
                <TableHead>
                    <TableRow>
                        {this._renderHeaderCell(FieldKey.origin, "Origin")}
                        {this._renderHeaderCell(FieldKey.pickupDate, "Pickup date")}
                        {this._renderHeaderCell(FieldKey.destination, "Destination")}
                        {this._renderHeaderCell(FieldKey.dropoffDate, "Dropoff date")}
                        {this._renderHeaderCell(FieldKey.price, "Price")}
                        {this._renderHeaderCell(FieldKey.miles, "Distance (mi)")}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {this.props.offers.map(offer => this._renderOfferRow(offer))}
                </TableBody>
            </Table>
        )
    }

    private _renderHeaderCell(key: FieldKey, title: string, numeric: boolean = false): JSX.Element {
        return (
            <TableCell numeric={numeric}>
                <TableSortLabel
                    active={this.props.sortField === key}
                    direction={this.props.sortDirection as any}
                    onClick={() => this.props.onSortToggled(key)} // Normally I would create a new component to avoid lambda here, but in the interest of time this is fine
                >
                    {title}
                </TableSortLabel>
            </TableCell>
        );
    }

    private _renderOfferRow(offer: IOffer): JSX.Element {
        return (
            <TableRow>
                <TableCell>
                    {offer.origin.city}, {offer.origin.state}
                </TableCell>
                {this._renderTimeCell(offer.origin.pickup)}
                <TableCell>
                    {offer.destination.city}, {offer.destination.state}
                </TableCell>
                {this._renderTimeCell(offer.destination.dropoff)}
                <TableCell numeric>
                    ${offer.offer.toFixed(2)}
                </TableCell>
                <TableCell numeric>
                    {offer.miles}
                </TableCell>
            </TableRow>
        )
    }

    private _renderTimeCell(time: ITime): JSX.Element {
        const startString: string = time.start.toLocaleDateString();
        const endString: string = time.end.toLocaleDateString();

        return (
            <TableCell>
                {startString}{startString !== endString && `- ${endString}`}
            </TableCell>
        );
    }
}