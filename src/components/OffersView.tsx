import './OffersView.css';

import AppBar from '@material-ui/core/AppBar';
import Paper from '@material-ui/core/Paper';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as React from 'react';
import { connect } from 'react-redux';
import { FieldKey, SortDirection } from 'src/Constants';
import { OffersActions } from 'src/redux/offers/OffersActions';
import {
    getCurrentPage,
    getCurrentPageError,
    getCurrentPageOfOffers,
    getSortDirection,
    getSortField,
} from 'src/redux/offers/OffersSelectors';

import { IOffer, IOffersApplicationState } from '../Contracts';
import { OffersTable } from './OffersTable';
import { Paginator } from './Paginator';

export interface IOffersViewProps {
    error?: Error;
    loading: boolean;
    offers: IOffer[];
    currentPage: number;
    sortField: FieldKey;
    sortDirection: SortDirection;
}

export class OffersView extends React.Component<IOffersViewProps & typeof Actions> {
    public render(): JSX.Element {
        const {
            currentPage,
            error,
            loading,
            offers,
            onPageLeft,
            onPageRight,
            onSortToggled,
            sortDirection,
            sortField
        } = this.props;

        return (
            <div>
                <AppBar position="sticky">
                    <Toolbar>
                        <Typography variant="h6" color="inherit">
                            Offers
                        </Typography>
                        <div className="paginator-container">
                            <Paginator pageNumber={currentPage} onLeft={onPageLeft} onRight={onPageRight} />
                        </div>
                    </Toolbar>
                </AppBar>

                <div className="offers-view">

                    <Paper>
                        <div className="offers-view-content">
                            {loading ?
                                <CircularProgress />
                                :
                                error ?
                                    "Server sent no response. Please refresh to view the latest"
                                    :

                                    <OffersTable
                                        offers={offers}
                                        sortDirection={sortDirection}
                                        sortField={sortField}
                                        onSortToggled={onSortToggled}
                                    />
                            }
                        </div>
                    </Paper>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: IOffersApplicationState): IOffersViewProps {
    return {
        error: getCurrentPageError(state),
        loading: state.offersState.loading,
        offers: getCurrentPageOfOffers(state),
        currentPage: getCurrentPage(state) + 1,
        sortDirection: getSortDirection(state),
        sortField: getSortField(state)
    }
}

const Actions = {
    onSortToggled: OffersActions.sortChanged,
    onPageLeft: OffersActions.pageLeft,
    onPageRight: OffersActions.pageRight
}

export const ConnectedOffersView = connect(mapStateToProps, Actions)(OffersView);

