export const enum FieldKey {
    pickupDate = "pickupDate",
    dropoffDate = "dropoffDate",
    price = "price",
    origin = "origin",
    destination = "destination",
    miles = "miles"
}

export const enum SortDirection {
    desc = "desc",
    asc = "asc"
}